{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs #-}

module Hi where

import Text.Shakespeare.I18N
import Yesod.Form
import Yesod.Core (HandlerFor)
import Data.Text (Text)
import qualified Data.Text as T

data App
data Site
type Handler = HandlerFor Site

instance RenderMessage Site FormMessage

bfs' :: a -> FieldSettings site
bfs' _ = FieldSettings { }
{-# NOINLINE bfs' #-}

data SurveyFormData = SurveyFormData {
    -- formLanguage :: Text,
    -- formTimezone :: Text,
    formSchool :: Maybe Text,
    formDegree :: Text,
    formGraduationYear :: Maybe Int, -- TODO
    -- formYearsInProgram :: Int, -- Always -1 now
    formMajor :: Maybe Text,
    formMinor :: Maybe Text,
    formDegreesHeld :: Maybe Text,
    formYearsOfExperience :: Maybe Int,
    formLanguages :: Maybe Text,
    formFavoriteLanguages :: Maybe Text,
    formExperienceClass :: Maybe Bool,
    formExperiencePersonal :: Maybe Bool,
    formExperienceInternship :: Maybe Bool,
--    formExperienceJob :: Bool,
    formYearsOfWork :: Maybe Int,
    formSoftwareEngineering :: Maybe Bool,
    formSecurityClass :: Maybe Bool,
    formSecurityTraining :: Maybe Bool,
    formSecurityExperience :: Maybe Bool,
    formPreviousContest :: Maybe Bool,
    formProgrammerRating :: Maybe Int, -- TODO
    formAttackerRating :: Maybe Int, -- TODO
    formAge :: Maybe Int,
    formGender :: Maybe Text,
    formNationality :: Maybe Text,
    formResumePermission :: Bool,
    formConfirmation :: Bool
    }


surveyForm :: Maybe SurveyFormData -> AForm Handler SurveyFormData -- FormRender m SurveyFormData
--surveyForm :: forall h site. (h ~ HandlerFor site, Monad h, RenderMessage site FormMessage)
--           => Maybe SurveyFormData -> AForm h SurveyFormData -- FormRender m SurveyFormData
surveyForm formM = SurveyFormData
    -- <*> areq timezoneField (bfs' "Timezone") (f formTimezone)
    <$> aopt textField (bfs' "University") (f formSchool) -- (Just "University of Maryland, College Park")
    <*> areq (selectFieldList degrees) (bfs' "Degree") (Just $ maybe "Undergraduate" formDegree formM)
    <*> aopt intField (bfs' "Graduation year") (f formGraduationYear)
    -- <*> areq intField (bfs' "Years in program (ex: Freshman is 1)") Nothing
    <*> aopt textField (bfs' "Major") (f formMajor)
    <*> aopt textField (bfs' "Minor") (f formMinor) -- (if any)
    <*> aopt textField (bfs' "Other degrees") (f formDegreesHeld)
    <*> aopt intField (bfs' "Years of programming experiences") (f formYearsOfExperience)
    <*> aopt textField (bfs' "What programming languages do you know?") (f formLanguages)
    <*> aopt textField (bfs' "What is your favorite programming language?") (f formFavoriteLanguages)
    <*> aopt boolField "Have you taken any programming classes?" (f formExperienceClass)
    <*> aopt boolField "Do you program for any personal projects?" (f formExperiencePersonal)
    <*> aopt boolField "Have you programmed during an internship?" (f formExperienceInternship)
--    <*> areq boolField' "Have you programmed during a full-time job?" Nothing
    <*> aopt intField (bfs' "How many years of work experience do you have coding?") (f formYearsOfWork)
    <*> aopt boolField "Have you taken a software engineering class?" (f formSoftwareEngineering)
    <*> aopt boolField "Have you taken a computer security class?" (f formSecurityClass)
    <*> aopt boolField "Have you had any formal training in computer security (aside from this coursera sequence)?" (f formSecurityTraining)
    <*> aopt boolField "Do you have prior experience in computer security?" (f formSecurityExperience)
    <*> aopt boolField "Have you participated in other security contests?" (f formPreviousContest)
    <*> aopt intField  (bfs' "How do you rate your abilities as a programmer on a scale from 1 (low) to 10 (high)?") (f formProgrammerRating)
    <*> aopt intField  (bfs' "How do you rate your abilities as an attacker on a scale from 1 (low) to 10 (high)?") (f formAttackerRating)
    <*> aopt intField (bfs' "Age") (f formAge)
    <*> aopt textField (bfs' "Gender") (f formGender)
    <*> aopt textField (bfs' "Nationality") (f formNationality)
    <*> areq boolField "Would you like us to make your CV available to companies who have sponsored our research, so they can contact you about future employment and internship opportunities?" (formResumePermission <$> formM)
    <*> areq checkBoxField "We ask that you allow us to use data gathered from your performance in this contest as part of a research study that aims to better understand how to build secure systems. Your identity will be held in strict confidence (unless you opt to share your information)." (Just $ maybe True formResumePermission formM)
    where
        degrees = [("Undergraduate"::Text, "Undergraduate"), ("Masters","Masters"),("Doctorate","Doctorate"), ("High school","High school"),("None","None")]

        f :: (SurveyFormData -> Maybe a) -> Maybe (Maybe a)
        f g = fmap (g) formM
